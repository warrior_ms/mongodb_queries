## Submitted By : Mustafa Sadriwala

## Assignment name : MongoDB Queries


Questions :
1. list of consultants having more than one submission.
2. list all the interviews of consultants.
3. list all PO of marketers.
4. unique vendor company name for which client location is the same.
5. count of consultants which submitted in the same city.
6. name of consultant and client who have been submitted on the same vendor.